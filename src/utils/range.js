/**
 * @function range This is a versatile function to create lists containing arithmetic progressions. 
 * It is most often used in for loops. The arguments must be plain integers. 
 * If the step argument is omitted, it defaults to 1. 
 * 
 * @example range(1, 3) will return [1, 2, 3]
 * @example range(5) will return [0, 1, 2, 3, 4, 5]
 * 
 * @param {Number} start where the array progression starts
 * @param {Number} end where the array progression ends
 * @param {Number} step 
 * @returns {Array} array 
 */
export const range = (start, end, step = 1) => {
    let array = [];

    if (start <= end) {
        for (let index = start; index <= end; index += step) { array.push(index)};
    }
    return array;
};