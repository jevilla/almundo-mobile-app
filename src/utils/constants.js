export const constants = {
    icons: {
        STAR: 'star',
        BATHROBES: 'bathrobes',
        BATHTUB: 'bathtub',
        BEACH_POOL_FACILITIES: 'beach-pool-facilities',
        BEACH: 'beach',
        BUSINESS_CENTER: 'business-center',
        CHILDREN_CLUB: 'children-club',
        COFFE_MAKER: 'coffe-maker',
        DEEP_SOAKING_BATHTUB: 'deep-soaking-bathtub',
        FITNESS_CENTER: 'fitness-center',
        GARDEN: 'garden',
        KITCHEN_FACILITIES: 'kitchen-facilities',
        NEWSPAPER: 'newspaper',
        NIGHTCLUB: 'nightclub',
        RESTAURANT: 'restaurant',
        SAFETY_BOX: 'safety-box',
        SEPARATE_BREDROOM: 'separate-bredroom',
        SHEETS: 'sheets',
        SEARCH: 'search'
    }
};
