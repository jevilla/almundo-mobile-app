import { Platform } from 'react-native';

export const strings = {
    PRICE_PER_BEDROOM_TEXT: 'Precio por noche por habitación',
    CURRENCY_ARS: 'ARS',
    CHECK_HOTEL: 'Ver hotel',
    FILTER: 'Filtrar ▼',
    ROOT_URL_IMAGES: 'https://almundo-api-test.herokuapp.com/images/hotels/',
    ROOT_URL_HOTELS: 'https://almundo-api-test.herokuapp.com/api/hotels',
    ROOT_URL_SEARCH_HOTEL: 'https://almundo-api-test.herokuapp.com/api/hotel/',
    ROOT_URL_SEARCH_HOTEL_BY_STARS: 'https://almundo-api-test.herokuapp.com/api/hotels/stars',
    ACCEPT_BUTTON_TEXT: 'Aceptar',
    PLACEHOLDER_FILTER_HOTEL: 'Ingrese el nombre del hotel',
    HOTEL_NAME: 'Nombre del Hotel',
    FILTER_BY_STARS: 'Estrellas',
    ALL_STARS: 'Todas las estrellas',
}
