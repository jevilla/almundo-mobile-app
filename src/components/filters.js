import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity, TextInput, CheckBox, LayoutAnimation, UIManager, Platform } from 'react-native';
import { strings, constants, range } from '../utils';
import { Icon } from '../components'
import _ from 'lodash';

class Filters extends Component {
    initialState = {
        filterShown: false,
        starsFilter: { 5: false, 4: false, 3: false, 2: false, 1: false },
        allStars: false,
        starsQuery: []
    }

    constructor(props) {
        super(props);
        this.hotelName = "";
        this.state = {
            ...this.initialState
        }
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental
            && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    openOrHideFilterSection = () => {
        this.setState({ filterShown: !this.state.filterShown }, () => {
            LayoutAnimation.easeInEaseOut();
        });
    }

    searchByHotelName = () => {
        if (!_.isEmpty(this.hotelName.trim())) {
            this.props.filterHotelName(this.hotelName);
            this.openOrHideFilterSection();
            this.setState({ ...this.initialState })
        }
    }

    setHotelName = (text) => this.hotelName = text;

    renderStars = (numberOfStars) => {
        return range(1, numberOfStars).map(number => {
            return <Icon key={number} type={constants.icons.STAR} />
        });
    }

    checkValueChange = (value, key) => {
        const starObject = Object.assign({}, this.state.starsFilter);
        starObject[key] = value;
        this.setState({ starsFilter: starObject, allStars: false }, () => {
            this.openOrHideFilterSection();
        });
        this.fillStarsQuery(starObject);
    }

    fillStarsQuery = (object) => {
        const starsQueryTemp = [];
        Object.keys(object).map(key => {
            if (object[key]) starsQueryTemp.push({ stars: +key });
        });
        if (!_.isEmpty(starsQueryTemp)) {
            this.props.getHotelsByStars(starsQueryTemp);
        }
    }

    starsFilterSection = () => {
        const {starsFilter} = this.state;

        return Object.keys(starsFilter).reverse().map((key, index) => {
            return (
                <View key={key} style={styles.checkBoxSection}>
                    <CheckBox
                        value={starsFilter[key]} style={styles.checkbox}
                        onValueChange={(value) => this.checkValueChange(value, key)}
                    />
                    {this.renderStars(key)}
                </View>
            );
        });
    }

    setAllStars = (value) => {
        this.setState({ allStars: value, starsFilter: this.initialState.starsFilter }, () => {
            this.openOrHideFilterSection()
        });
        if (value) {
            this.props.getAllHotels();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity 
                    style={styles.actionFilter}
                    onPress={this.openOrHideFilterSection}
                >
                    <Text style={styles.fontFilter}>{strings.FILTER}</Text>
                </TouchableOpacity>
                {this.state.filterShown &&
                    <React.Fragment>
                        <View style={styles.filterByHotelNameContainer}>
                            <View style={styles.searchContainer}>
                                <Icon type={constants.icons.SEARCH} />
                                <Text style={styles.hotelName}>{strings.HOTEL_NAME}</Text>
                            </View>
                            <View style={styles.fieldContainer}>
                                <TextInput 
                                    style={styles.hotelNameField}
                                    placeholder={strings.PLACEHOLDER_FILTER_HOTEL}
                                    onChangeText={this.setHotelName}
                                />
                                <TouchableOpacity 
                                    style={styles.acceptButtonAction}
                                    onPress={this.searchByHotelName}
                                >
                                    <Text style={styles.acceptButtonText}>{strings.ACCEPT_BUTTON_TEXT}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.filterByStarsContainer}>
                            <View style={styles.searchContainer}>
                                <Icon type={constants.icons.STAR} />
                                <Text style={styles.hotelName}>{strings.FILTER_BY_STARS}</Text>
                            </View>
                            <View style={styles.checkBoxSectionContainer}>
                                <View style={styles.checkBoxSection}>
                                    <CheckBox
                                        value={this.state.allStars} style={styles.checkbox}
                                        onValueChange={this.setAllStars}
                                    />
                                    <Text style={styles.allStars}>{strings.ALL_STARS}</Text>
                                </View>
                                {this.starsFilterSection()}
                            </View>
                        </View>
                    </React.Fragment>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fontFilter: {
        fontSize: 18,
        color: '#107ab4',
        fontWeight: 'bold'
    },
    actionFilter: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    filterByHotelNameContainer: {
        justifyContent: 'space-around',
        width: '100%',
        height: 100
    },
    filterByStarsContainer: {
        marginTop: 10,
        justifyContent: 'space-around',
        width: '100%',
    },
    fieldContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    searchContainer: {
        flexDirection: 'row',
        marginLeft: 10,
        width: '100%'
    },
    hotelNameField: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 2,
        width: '70%',
        height: 40,
        fontSize: 15,
        backgroundColor: '#f0f0f0'
    },
    hotelName: {
        fontSize: 16,
        fontWeight: '500',
        color: '#107ab4',
    },
    acceptButtonAction: {
        height: 40,
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        backgroundColor: 'rgb(0, 41, 121)',
    },
    acceptButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500'
    },
    checkBoxSectionContainer: {
        marginTop: 15,
        marginLeft: 5
    },
    checkBoxSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    checkbox: {
        marginRight: 10,
    },
    allStars: {
        fontSize: 16,
        fontWeight: '600'
    }
})

export { Filters };