import React, { Component } from 'react';
import { Text, StyleSheet, View, Dimensions, Image, Platform, TouchableOpacity } from 'react-native';
import { Icon } from '../components'
import { range, strings, constants } from '../utils'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class HotelCard extends Component {

    _keyStractor = () => {
        const {name, stars, id} = this.props;
        return id + name + stars;
    }

    hotelStars = (stars) => range(1, stars).map(n => <Icon key={n} type={constants.icons.STAR} />);

    hotelAmenities = (amenities) => {
        return amenities.map((amenity, i) => {
            return (
                <Icon key={i} type={amenity} />
            );
        });
    }

    render() {
        const {amenities, image, name, price, stars} = this.props;
        const uri = strings.ROOT_URL_IMAGES.concat(image);

        return (
            <View style={styles.container}>
                <View style={styles.hotelImageContainer}>
                    <Image style={styles.hotelImage} source={{uri}} resizeMode={"cover"} />
                </View>
                <View style={styles.hotelInfo}>
                    <View style={styles.nameContainer}>
                        <Text style={styles.hotelName}>{name}</Text>
                    </View>
                    <View style={styles.starsContainer}>
                        {this.hotelStars(stars, name)}
                    </View>
                    <View style={styles.amenitiesContainer}>
                        {this.hotelAmenities(amenities)}
                    </View>
                    <View style={styles.bookingContainer}>
                        <Text>{strings.PRICE_PER_BEDROOM_TEXT}</Text>
                        <View style={styles.priceContainer}>
                            <Text style={styles.currency}>{strings.CURRENCY_ARS}</Text>
                            <Text style={styles.amount}>{price}</Text>
                        </View>
                        <TouchableOpacity style={styles.actionButton}>
                            <Text style={styles.actionText}>{strings.CHECK_HOTEL}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 3,
        height: windowHeight * 0.77,
        width: windowWidth * 0.95,
        borderRadius: 2,
        alignItems: 'center',
        marginVertical: 10,
    },
    hotelImageContainer: {
        width: '95%',
        height: '43%',
        paddingVertical: 10,
        alignItems: 'center',
    },
    hotelImage: {
        width: '100%', 
        height: '100%'
    },
    hotelName: {
        fontSize: 22,
        color: '#107ab4',
        fontWeight: 'bold'
    },
    hotelInfo: {
        width: '95%',
        height: '50%'
    },
    nameContainer: {
        marginVertical: 5
    },
    starsContainer: {
        marginVertical: 5,
        flexDirection: 'row',
    },
    amenitiesContainer: {
        marginVertical: 8,
        flexDirection: 'row',
    },
    bookingContainer: {
        marginTop: 10,
        alignItems: 'center',
    },
    priceContainer: {
        flexDirection: 'row',
        marginTop: 5,
        alignItems: 'center'
    },
    currency: {
        color: 'rgb(225, 105, 0)',
        marginRight: 5,
        fontSize: 20
    },
    amount: {
        color: 'rgb(225, 105, 0)',
        fontSize: 35,
        fontWeight: '800'
    }, 
    actionButton: {
        marginTop: 10,
        width: '100%',
        alignItems: 'center',
        backgroundColor: 'rgb(0, 41, 121)',
        paddingVertical: 10,
        borderRadius: 5
    },
    actionText: {
        color: '#ffffff',
        fontSize: 25,
        fontWeight: '600'
    }
});

export { HotelCard };