import React, { Component } from 'react';
import { Text, StyleSheet, View, FlatList, Platform } from 'react-native';
import { Filters } from '../components/filters';
import { HotelCard } from '../components/hotelCard';
import { strings } from '../utils';

import axios from 'axios';

class Hotel extends Component {
    constructor(props) {
        super(props);
        this.url = strings.ROOT_URL_HOTELS;
        this.state = {
            hotels: []
        };
    }

    componentDidMount() {
        // TODO: axios request should be done on a different file, separate concerns 
        this.getAllHotels();
    }

    byHotelNameRequest = (hotelName) => {
        axios({
            method: 'get',
            url: strings.ROOT_URL_SEARCH_HOTEL.concat(hotelName),
            headers: { Accept: 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).then((response) => {
            console.log(response.data);
            this.setState({ hotels: response.data })
        }).catch(err => console.error(err));
    }

    getAllHotels = () => {
        axios({
            method: 'get',
            url: this.url,
            headers: { Accept: 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).then((response) => {
            console.log(response.data);
            this.setState({ hotels: response.data })
        }).catch(err => console.error(err));
    }

    getHotelsByStars = (stars) => {
        axios({
            method: 'post',
            url: strings.ROOT_URL_SEARCH_HOTEL_BY_STARS,
            headers: { Accept: 'application/json' },
            data: { stars }
        }).then((response) => {
            console.log(response.data);
            this.setState({ hotels: response.data })
        }).catch(err => console.error(err));
    }

    _keyExtractor = (item) => item.id;

    hotelList = () => {
        const { hotels } = this.state;

        return (
            <FlatList
                data={hotels}
                renderItem={({item}) => <HotelCard {...item} />}
                showsVerticalScrollIndicator={false}
                keyExtractor={this._keyExtractor}
            >
            </FlatList>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Filters 
                    filterHotelName={this.byHotelNameRequest}
                    getAllHotels={this.getAllHotels}
                    getHotelsByStars={this.getHotelsByStars}
                />
                <View style={styles.cardListContainer}>
                    {this.hotelList()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(230, 230, 230)'
    },
    cardListContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export { Hotel };
