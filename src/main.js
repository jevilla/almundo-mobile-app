import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, SafeAreaView } from 'react-native';
import { Header } from './components/header';
import { Hotel } from './views'

export default class App extends Component {
    render() {
        return (
            // TODO: Implement react navigation instead of hardcoding the hotel as initial view
            <View style={styles.container}>
                <StatusBar hidden/>
                <Header />
                <Hotel />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%'
    }
});