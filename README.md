This project was created with [React Native](https://facebook.github.io/react-native/) using the script `react-native init [my-project-name]`

To be able to run this project it is required have installed node and npm and the react-native-cli, and config Android Studio and Xcode. For more info please visit this [page](https://facebook.github.io/react-native/docs/getting-started).

After download or clone the repo you should run the following scripts in order to run the app.
## Intall the node_modules:
use `yarn` or `npm install`.

## Run the app

- Open a simulator from Android studio

- In the project directory, you can run `react-native run-android` or `react-native run-ios`

### `After following previous steps you should see the app running: `

# You can download the Android APK in this [site](https://1drv.ms/u/s!Al4QVGVI3pfkmx4bs-SPZxFBKt31?e=HSoedk)

![picture](almundo1.gif)
![picture](almundo2.gif)